"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

def show(sound, newline: bool):

    if newline == True:

        for i in range(len(sound)):

            print(sound[i])

    elif newline == False:

        for i in range(len(sound)):

            print(sound[i], end=", ")

def info(sound):

    # Crea variables donde se van a guardar los valores necesarios de contar

    media = sum(sound)//len(sound)
    vpositivos = 0
    vnegativos = 0
    vnulos = 0

    for nsamples in(sound):  # Bucle para contar los valores + - y nulos dentro de la lista

        if nsamples > 0:
            vpositivos += 1
        elif nsamples < 0:
            vnegativos += 1
        else:
            vnulos += 1



    print(f'samples: {len(sound)}\nMax value: {max(sound)} (sample {sound.index(max(sound))})\n'
            f'Min value: {min(sound)} (samples: {sound.index(min(sound))})\nMean value: {media}\n'
            f'positive samples: {vpositivos}\nnegative samples: {vnegativos}\nnull samples: {vnulos}')
