
import sources
import sinks
import processors


def main():

    # Fuentes:

    x = 0
    while x == 0:

        source = input("\nEscriba una fuente (load, sin, constant, square): ")

        if source == 'sin':

            nsamples1 = input('nº samples: ')

            freq = int(input('frecuencia: '))

            sound = sources.sin(int(nsamples1), int(freq))
            x = 1

        elif source == 'constant':

            nsamples2 = input('nº samples: ')

            level = int(input('nivel para constant: '))

            sound = sources.constant(int(nsamples2), int(level))

            x = 1

        elif source == 'square':

            nsamples3 = int(input('nº samples: '))

            nperiod = int(input('periodo: '))

            sound = sources.square(int(nsamples3), int(nperiod))

            x = 1

        elif source == 'load':

            str =input('Dime el string para load (dirección del archivo): ')

            sound = sources.load(str)
            x = 1


    while x == 1:

        processor = input(f'Dime un procesador (ampli, shift, trim, repeat, clean, round): ')

        sink = input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin':

            print(f'Ejecutando sin({nsamples1} {freq}) y {processor} con {sink}. Veamos los parámetros: ')

        if source == 'constant':

            print(f'Ejecutando constant({nsamples2} {level}) y {processor} con {sink}. Veamos los parámetros: ')

        if source == 'square':

            print(f'Ejecutando square({nsamples3} {nperiod}) y {processor} con {sink}. Veamos los parámetros: ')

        if source == 'load':

            print(f'Ejecutando load y {processor} con {sink}. Veamos los parámetros: ')


        if processor == 'ampli':

            processor_arg1 = float(input('Introduce un factor para ampliar: '))

            sound = processors.ampli(sound, factor=processor_arg1)

            x = 0

        elif processor == 'shift':

            processor_arg1 = int(input('Introduce un valor de desplazamiento: '))

            sound = processors.shift(sound, value=processor_arg1)

            x = 0

        elif processor == 'trim':

            processor_arg1 = int(input('Introduce un valor de reducción: '))

            processor_arg2 = input(f'Introduce "True" para quitar muestras del principio, o "False" para '
                                   f'quitar valores del final: ')

            sound = processors.trim(sound, reduction=int(processor_arg1), start=bool(processor_arg2))

            x = 0

        elif processor == 'repeat':

            processor_arg1 = int(input('Introduce un valor de repetición: '))

            sound = processors.repeat(sound, factor=int(processor_arg1))

            x = 0

        elif processor == 'clean':

            processor_arg1 = int(input('Introduce un valor para "limpiar": '))

            sound = processors.clean(sound, level=int(processor_arg1))

            x = 0

        elif processor == 'round':

            sound = processors.round(sound)

            x = 0



        'sumideros'

        if sink == 'play':

            sinks.play(sound)
            x = 0

        if sink == 'draw':

            maximo_caracter = int(input('Numero máximo: '))

            sinks.draw(sound, maximo_caracter)

            x = 0

        if sink == 'show':

            newline_ask = input('¿Números con salto de linea? (si - no): ')

            if newline_ask == 'si':

                sinks.show(sound, newline=True)

            else:

                sinks.show(sound, newline=False)

            x = 0

        if sink == 'info':

            sinks.info(sound)

            x = 0


if __name__ == '__main__':
    main()
