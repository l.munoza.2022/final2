import sys

import sources
import sinks
import processors


def get_bool_from_string(str):
    str = str.lower()
    if str == 'true'.lower():
        return True
    elif str == 'false'.lower():
        return False
    else:
        mensaje_error="Error parametro True o False"
        print(mensaje_error)
        sys.exit(mensaje_error)


def main():
    source = sys.argv[1]
    source_arg1 = sys.argv[2]


    if source == 'sin':

        source_arg2 = sys.argv[3]
        sound1 = sources.sin(nsamples=int(source_arg1), freq=int(source_arg2))
        x = 3

    elif source == 'constant':

        source_arg2 = sys.argv[3]
        sound1 = sources.constant(nsamples=int(source_arg1), level=int(source_arg2))
        x = 3

    elif source == 'square':

        source_arg2 = sys.argv[3]
        sound1 = sources.sin(nsamples=int(source_arg1), freq=int(source_arg2))
        x = 3

    elif source == 'load':

        sound1 = sources.load(path=source_arg1)
        x = 2

    else:

        sys.exit('ERROR')

    source = sys.argv[x + 1]
    source_arg1 = sys.argv[x + 2]

    if source == 'sin':

        source_arg2 = sys.argv[x + 3]
        sound2 = sources.sin(nsamples=int(source_arg1), freq=int(source_arg2))
        x += 3

    elif source == 'constant':

        source_arg2 = sys.argv[x + 3]
        sound2 = sources.constant(nsamples=int(source_arg1), level=int(source_arg2))
        x += 3

    elif source == 'square':

        source_arg2 = sys.argv[x + 3]
        sound2 = sources.sin(nsamples=int(source_arg1), freq=int(source_arg2))
        x += 3

    elif source == 'load':

        sound2 = sources.load(path=source_arg1)
        x += 2

    else:

        sys.exit('ERROR')


    sound = processors.add(sound1, sound2)

    processor = sys.argv[x + 1]

    if processor == 'ampli':

        processor_arg1 = sys.argv[x + 2]
        sound = processors.ampli(sound, factor=float(processor_arg1))
        x += 2

    elif processor == 'shift':

        processor_arg1 = sys.argv[x + 2]
        sound = processors.shift(sound, value=int(processor_arg1))
        x += 2

    elif processor == 'trim':

        processor_arg1 = sys.argv[x + 2]
        processor_arg2 = sys.argv[x + 3]
        sound = processors.trim(sound, reduction=int(processor_arg1), start=get_bool_from_string(processor_arg2))
        x += 3

    elif processor == 'repeat':

        processor_arg1 = sys.argv[x + 2]
        sound = processors.repeat(sound, factor=int(processor_arg1))
        x += 2

    elif processor == 'round':

        sound = processors.round(sound)
        x += 1


    elif processor =='clean':


        processor_arg1 = sys.argv[x + 2]
        sound = processors.clean(sound, level=int(processor_arg1))
        x+=2


    sink = sys.argv[x + 1]

    if sink == 'play':

        sinks.play(sound)

    elif sink == 'draw':

        sink_arg1 = sys.argv[x + 2]
        sinks.draw(sound, max_chars=int(sink_arg1))

    elif sink == 'show':

        sink_arg1 = sys.argv[x + 2]
        sinks.show(sound, newline=get_bool_from_string(sink_arg1))

    elif sink == 'info':
        sinks.info(sound)


if __name__ == '__main__':
    main()
