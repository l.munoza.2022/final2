"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config
import sound


def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    '''out_sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        out_sound[nsample] = int(value)
    return out_sound'''


    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)
    return sound


def shift(sound, value:int):

    out_sound = [0] * len(sound)

    for nsample in range(len(sound)):

        desplazamiento = sound[nsample] + value

        if desplazamiento > config.max_amp:
            desplazamiento = config.max_amp

        elif desplazamiento < -config.max_amp:
            desplazamiento = -config.max_amp

        out_sound[nsample] = int(desplazamiento)

    return out_sound


def trim(sound, reduction: int, start: bool):

    n = reduction

    if start:

        if n >= len(sound):
            return []
        else:
            sound=  sound[n:]
    else:
        if n >= len(sound):
            return []
        else:
            sound =  sound[:-n]

    return sound


def repeat(sound, factor: int):

    if factor > 0:

        out_sound = sound * factor

        return out_sound

    else:

        return sound

def round(sound):

    out_sound = [0] * len(sound)

    for nsample in range(len(sound)):

        if nsample < 1:

            out_sound[nsample] = int((sound[nsample] + sound[nsample+1])/2)

        elif nsample == len(sound)-1:

            out_sound[nsample] =  int((sound[nsample] + sound[nsample -1]) / 2)

        else:

            out_sound[nsample]= int((sound[nsample-1] +sound[nsample] + sound[nsample + 1]) /3)

    sound = out_sound
    return sound

def clean(sound, level: int):

    for nsample in range(len(sound)):

        if abs(sound[nsample]) < level:

            sound[nsample]=0

    return sound

def add(sound1, sound2):


    if len(sound1) >= len(sound2):

        for nsample in range(len(sound2)):

            value = sound1[nsample] + sound2[nsample]

            if value >= config.max_amp:

                sound1[nsample] = config.max_amp

            elif value <= -config.max_amp:

                sound1[nsample] = -config.max_amp

            else:

                sound1[nsample] = value

        sound = sound1

    elif len(sound1) < len(sound2):

        for nsample in range(len(sound1)):

            value = sound1[nsample] + sound2[nsample]

            if value >= config.max_amp:

                sound2[nsample] = config.max_amp

            elif value <= -config.max_amp:

                sound2[nsample] = -config.max_amp

            else:

                sound2[nsample] = value

        sound = sound2


    return sound







