REQUISITOS MÍNIMOS:

- Método constant
- Método square
- Método show
- Método info
- Método shift
- Método trim
- Método repeat
- Creación sound.py
- Creación menu.py


REQUISITOS OPCIONALES:

- Método clean
- Método round
- Creación de sound2.py
- Creación de menu2.py
- Creación de menu3.py
- Creación de add
- Creación de soundadd.py