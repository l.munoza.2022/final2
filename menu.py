
import sources
import sinks


def main():

    x = 0
    while x == 0:
        source = input("\nDime una fuente (load, sin, constant, square): ")

        if source == 'sin':

            nsamples1 = input('Dame los párametros para sin: ')

            freq = int(input('frecuencia: '))

            sound = sources.sin(int(nsamples1), int(freq))

            x = 1

        elif source == 'constant':

            nsamples2 = input('Dame los parámetros para constant: ')

            level = int(input('nivel para constant: '))

            sound = sources.constant(int(nsamples2), int(level))

            x = 1

        elif source == 'square':

            nsamples3 = int(input('Dame los parámetros para square: '))

            nperiod = int(input('Escribe el periodo: '))

            sound = sources.square(int(nsamples3), int(nperiod))
            x = 1

        elif source == 'load':

            str =input('Dime el string para load (dirección del archivo): ')

            sound = sources.load(str)
            x = 1


    while x==1:

        sink = input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin':

            print(f'Ejecutando sin({nsamples1} {freq}) y {sink}')

        if source == 'constant':
            print(f'Ejecutando constant({nsamples2} {level}) y {sink}')

        if source == 'square':
            print(f'Ejecutando square({nsamples3} {nperiod}) y {sink}')

        if source == 'load':
            print(f'Ejecutando load y {sink}')



        if sink == 'play':

            sinks.play(sound)
            x = 0

        if sink == 'draw':
            maximo_caracter = int(input('Numero maximo: '))  # no poner números pequeños
            sinks.draw(sound, maximo_caracter)

            x = 0

        if sink == 'show':

            newline_ask = input('¿Números con salto de linea? (si - no): ')

            if newline_ask == 'si':
                sinks.show(sound, newline=True)

            else:
                sinks.show(sound, newline=False)

            x = 0

        if sink == 'info':

            sinks.info(sound)





if __name__ == '__main__':
    main()
